# Thai translation of debconf.
# Copyright (C) 2006-2010 Software in the Public Interest, Inc.
# This file is distributed under the same license as the debconf package.
# Theppitak Karoonboonyanan <thep@linux.thai.net>, 2006-2010.
#
msgid ""
msgstr ""
"Project-Id-Version: debconf\n"
"Report-Msgid-Bugs-To: debconf@packages.debian.org\n"
"POT-Creation-Date: 2009-08-24 19:24+0200\n"
"PO-Revision-Date: 2010-02-04 17:48+0700\n"
"Last-Translator: Theppitak Karoonboonyanan <thep@linux.thai.net>\n"
"Language-Team: Thai <thai-l10n@googlegroups.com>\n"
"Language: th\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Choices
#: ../templates:1001
msgid "Dialog"
msgstr "กล่องโต้ตอบ"

#. Type: select
#. Choices
#: ../templates:1001
msgid "Readline"
msgstr "อ่านจากบรรทัด"

#. Type: select
#. Choices
#: ../templates:1001
msgid "Editor"
msgstr "แก้ไขข้อความ"

#. Type: select
#. Choices
#: ../templates:1001
msgid "Noninteractive"
msgstr "ไม่โต้ตอบ"

#. Type: select
#. Description
#: ../templates:1002
msgid "Interface to use:"
msgstr "อินเทอร์เฟซที่จะใช้:"

#. Type: select
#. Description
#: ../templates:1002
msgid ""
"Packages that use debconf for configuration share a common look and feel. "
"You can select the type of user interface they use."
msgstr ""
"แพกเกจต่างๆ ที่ใช้ debconf ในการตั้งค่า จะมีรูปลักษณ์และการใช้งานเหมือนๆ กัน "
"คุณสามารถเลือกชนิดของการติดต่อผู้ใช้ที่จะใช้ได้"

#. Type: select
#. Description
#: ../templates:1002
msgid ""
"The dialog frontend is a full-screen, character based interface, while the "
"readline frontend uses a more traditional plain text interface, and both the "
"gnome and kde frontends are modern X interfaces, fitting the respective "
"desktops (but may be used in any X environment). The editor frontend lets "
"you configure things using your favorite text editor. The noninteractive "
"frontend never asks you any questions."
msgstr ""
"การติดต่อผ่านกล่องโต้ตอบ เป็นอินเทอร์เฟซเต็มจอในโหมดตัวอักษร "
"ในขณะที่การติดต่อแบบอ่านจากบรรทัด (readline) เป็นอินเทอร์เฟซแบบดั้งเดิมในโหมดตัวอักษร "
"และการติดต่อทั้งของ GNOME และ KDE จะใช้อินเทอร์เฟซแบบกราฟิกส์ผ่าน X สมัยใหม่ "
"ตามเดสก์ท็อปที่ใช้ (แต่ก็สามารถใช้ในสภาพแวดล้อม X ใดๆ ก็ได้) การติดต่อแบบแก้ไขข้อความ "
"จะให้คุณตั้งค่าต่างๆ โดยใช้เครื่องมือแก้ไขข้อความที่คุณเลือกไว้ ส่วนการติดต่อแบบไม่โต้ตอบ "
"จะไม่ถามคำถามใดๆ กับคุณเลย"

#. Type: select
#. Choices
#: ../templates:2001
msgid "critical"
msgstr "วิกฤติ"

#. Type: select
#. Choices
#: ../templates:2001
msgid "high"
msgstr "สูง"

#. Type: select
#. Choices
#: ../templates:2001
msgid "medium"
msgstr "กลาง"

#. Type: select
#. Choices
#: ../templates:2001
msgid "low"
msgstr "ต่ำ"

#. Type: select
#. Description
#: ../templates:2002
msgid "Ignore questions with a priority less than:"
msgstr "ไม่ต้องถามคำถามที่มีระดับความสำคัญต่ำกว่า:"

#. Type: select
#. Description
#: ../templates:2002
msgid ""
"Debconf prioritizes the questions it asks you. Pick the lowest priority of "
"question you want to see:\n"
"  - 'critical' only prompts you if the system might break.\n"
"    Pick it if you are a newbie, or in a hurry.\n"
"  - 'high' is for rather important questions\n"
"  - 'medium' is for normal questions\n"
"  - 'low' is for control freaks who want to see everything"
msgstr ""
"debconf จะจัดระดับความสำคัญของคำถามที่จะถามคุณ "
"กรุณาเลือกระดับความสำคัญของคำถามที่ต่ำที่สุดที่คุณต้องการเห็น:\n"
"  - 'วิกฤติ' จะถามคุณเฉพาะคำถามที่คำตอบมีโอกาสทำให้ระบบพังได้\n"
"    คุณอาจเลือกตัวเลือกนี้ถ้าคุณเป็นมือใหม่ หรือกำลังรีบ\n"
"  - 'สูง' สำหรับคำถามที่สำคัญพอสมควร\n"
"  - 'กลาง' สำหรับคำถามปกติ\n"
"  - 'ต่ำ' สำหรับผู้อยากรู้อยากเห็นที่อยากปรับละเอียดทุกตัวเลือก"

#. Type: select
#. Description
#: ../templates:2002
msgid ""
"Note that no matter what level you pick here, you will be able to see every "
"question if you reconfigure a package with dpkg-reconfigure."
msgstr ""
"สังเกตว่า ไม่ว่าคุณจะเลือกระดับคำถามใดตรงนี้ คุณจะยังเห็นคำถามทุกข้อถ้าคุณตั้งค่าแพกเกจใหม่ด้วย "
"dpkg-reconfigure"

#. Type: text
#. Description
#: ../templates:3001
msgid "Installing packages"
msgstr "กำลังติดตั้งแพกเกจ"

#. Type: text
#. Description
#: ../templates:4001
msgid "Please wait..."
msgstr "กรุณารอสักครู่..."

#. Type: text
#. Description
#. This string is the 'title' of dialog boxes that prompt users
#. when they need to insert a new medium (most often a CD or DVD)
#. to install a package or a collection of packages
#: ../templates:6001
msgid "Media change"
msgstr "เปลี่ยนแผ่น"
